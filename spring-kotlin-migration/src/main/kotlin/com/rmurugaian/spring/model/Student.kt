package com.rmurugaian.spring.model

import java.time.LocalDate

/**
 * @author rmurugaian 2018-10-22
 */
data class Student(val id: String?, val name: String, val dob: LocalDate)